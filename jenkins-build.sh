#!/bin/bash -xe

npm config set strict-ssl false

LOCAL_VERSION=$(node -p -e "require('./package.json').version"); # Version in package.json
REMOTE_VERSION=$(npm view . version); # Version was published
echo $LOCAL_VERSION
echo $REMOTE_VERSION

if [ "${LOCAL_VERSION}" == "${REMOTE_VERSION}" ] || [ "${LOCAL_VERSION}" \< "${REMOTE_VERSION}" ]
then # if/then branch
  	npm install
    npm run build-wc
    echo "The version was not changed, this will not be published."
else # else branch
	npm install
    npm run build-wc
    npm publish
fi
